require_relative './markup/markup'
require_relative './markup/flat'
require_relative './markup/material'
require_relative './markup/people'
require_relative './markups_calculator'

class NuPackEstimator

  attr_accessor :base_cost, :people_count, :material

  def initialize(opts = {})
    @base_cost = opts[:base_cost]
    @people_count = opts[:people_count]
    @material = opts[:material]
  end

  def cost_with_markups
    calculator = MarkupsCalculator.new(input_values)
    one_plus_percentage_flat = calculator.one_plus_percentage(Markup::Flat)
    one_plus_percentage_additional = calculator.one_plus_percentage(Markup::Material, Markup::People)

    cost = @base_cost * one_plus_percentage_flat * one_plus_percentage_additional
    cost.round(2)
  end

  def self.cost_with_markups(base_cost, people_count, material)
    estimator = self.new({
      base_cost: base_cost,
      people_count: people_count,
      material: material
    })
    estimator.cost_with_markups
  end

  private

  def input_values
    {
      base_cost: @base_cost,
      people_count: @people_count,
      material: @material
    }
  end

end

