# NuPack Implementation
by Jon Erik Suero

### How to get the final cost with markups:

0. Require the class

    ```ruby
    # Example:
    require './nu_pack_estimator'
    ```

0.  Option #1: Use the provided class method
     `NuPackEstimator.cost_with_markups(<base_cost>, <people_count>, <material>)`
    ```ruby
    # Example:
    NuPackEstimator.cost_with_markups(1299.99, 3, 'food') # 1591.58
    ```

0.  Option #2: Create a `NuPackEstimator` instance, then use  `<estimator_instance>.cost_with_markups`
    ```ruby
    # Example # 1:
    estimator = NuPackEstimator.new({
        base_cost: 1299.99,
        people_count: 3,
        material: 'food'
    })
    estimator.cost_with_markups # 1591.58

    # Example # 2:
    estimator = NuPackEstimator.new
    estimator.base_cost = 1299.99
    estimator.people_count = 3
    estimator.material = 'food'
    estimator.cost_with_markups # 1591.58
    ```

### How to run the tests:

0. Go to the project's directory
1. Run `rspec`
