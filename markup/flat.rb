class Markup::Flat < Markup

  BASE_PERCENTAGE = 0.05

  def initialize(opts)
    @base_cost = opts[:base_cost]
  end

  def percentage
    BASE_PERCENTAGE
  end

  def validate
    raise ArgumentError.new("Base Cost must be a number.") unless (@base_cost.is_a? Numeric)
    raise ArgumentError.new("Base Cost must be greater than or equal to zero.") if (@base_cost < 0)
  end

end

