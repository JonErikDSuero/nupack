class Markup::Material < Markup

  BASE_PERCENTAGES = {
    'drugs' => 0.075,
    'electronics' => 0.02,
    'food' => 0.13,
    'other' => 0
  }

  def initialize(opts)
    @material = opts[:material]
  end

  def percentage
    BASE_PERCENTAGES[@material.downcase] || BASE_PERCENTAGES['other']
  end

  def validate
    raise ArgumentError.new("Material must be a string.") unless (@material.is_a? String)
  end

end

