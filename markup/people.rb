class Markup::People < Markup

  BASE_PERCENTAGE = 0.012

  def initialize(opts)
    @people_count = opts[:people_count]
  end

  def percentage
    BASE_PERCENTAGE * @people_count
  end

  def validate
    raise ArgumentError.new("Number of People must be a whole number.") unless (@people_count.is_a? Integer)
    raise ArgumentError.new("Number of People must be greater than zero.") if (@people_count <= 0)
  end

end

