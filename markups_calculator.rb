class MarkupsCalculator

  def initialize(opts)
    @opts = opts
  end

  def one_plus_percentage(*markup_classes)
    markups = setup_markups(markup_classes)
    1 + markups.map(&:percentage).reduce(:+)
  end

  private

  def setup_markups(markup_classes)
    markup_classes.map do |klass|
      markup = klass.new(@opts)
      markup.validate
      markup
    end
  end

end

