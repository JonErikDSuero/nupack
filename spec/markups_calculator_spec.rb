require 'spec_helper'

describe MarkupsCalculator do
  let(:opts) { {} }
  let(:calculator) { described_class.new(opts) }

  describe '#one_plus_percentage' do

    context 'markup raises' do
      it 'should propagate the exception' do
        expect_any_instance_of(Markup::Flat).to receive(:validate)
        expect_any_instance_of(Markup::People).to receive(:validate).and_raise(RuntimeError)
        expect{ calculator.one_plus_percentage(Markup::Flat, Markup::People) }.to raise_error(RuntimeError)
      end
    end

  end
end

