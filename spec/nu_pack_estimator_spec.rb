require 'spec_helper'

describe NuPackEstimator do
  let(:base_cost) { 1299.99 }
  let(:base_cost_invalid) { -1 }
  let(:people_count) { 3 }
  let(:people_count_invalid) { 0 }
  let(:material) { 'food' }
  let(:material_invalid) { nil }
  let(:values) { { base_cost: base_cost, people_count: people_count, material: material } }

  let(:estimator) { described_class.new(values) }

  describe '#cost_with_markups' do
    let(:estimator) { described_class.new(values) }

    context 'valid values' do
      it 'should calculate cost with markups' do
        expect(estimator.cost_with_markups).to eq(1591.58)
      end
    end

    context 'invalid values' do
      subject { lambda{estimator.cost_with_markups} }

      context 'invalid base_cost' do
        let(:base_cost) { base_cost_invalid }
        it { should raise_error(ArgumentError, 'Base Cost must be greater than or equal to zero.') }
      end

      context 'invalid people_count' do
        let(:people_count) { people_count_invalid }
        it { should raise_error(ArgumentError, 'Number of People must be greater than zero.') }
      end

      context 'invalid material' do
        let(:material) { material_invalid }
        it { should raise_error(ArgumentError, 'Material must be a string.') }
      end
    end
  end

  describe '.cost_with_markups' do
    it 'should calculate cost with markups' do
      expect(described_class.cost_with_markups(1299.99, 3, 'food')).to eq(1591.58)
      expect(described_class.cost_with_markups(5432.00, 1, 'drugs')).to eq(6199.81)
      expect(described_class.cost_with_markups(12456.95, 4, 'books')).to eq(13707.63)
    end

    it 'should create an instance and estimate/evaluate the cost' do
      expect_any_instance_of(described_class).to receive(:cost_with_markups)
      described_class.cost_with_markups(1299.99, 3, 'food')
    end

    it 'should propagate the exception if application of markups failed' do
      expect_any_instance_of(described_class).to receive(:cost_with_markups).and_raise(RuntimeError)
      expect{ described_class.cost_with_markups(1299.99, 3, 'food') }.to raise_error(RuntimeError)
    end
  end
end

