require 'spec_helper'

describe Markup::Material do
  let(:markup) { described_class.new(material: material) }

  describe '#percentage' do
    subject { markup.percentage }

    context 'drugs' do
      let(:material) { 'drugs' }
      it { should eq(0.075) }
    end

    context 'electronics' do
      let(:material) { 'electronics' }
      it { should eq(0.02) }
    end

    context 'food' do
      let(:material) { 'food' }
      it { should eq(0.13) }
    end

    context 'has uppercase letters' do
      let(:material) { 'Electronics' }
      it { should eq(0.02) }
    end

    context 'other' do
      let(:material) { 'Plutonium-238' }
      it { should eq(0) }
    end
  end

  describe '#validate' do
    subject { lambda { markup.validate } }

    context 'valid material' do
      context 'drugs' do
        let(:material) { 'drugs' }
        it { should_not raise_error }
      end

      context 'electronics' do
        let(:material) { 'electronics' }
        it { should_not raise_error }
      end

      context 'food' do
        let(:material) { 'food' }
        it { should_not raise_error }
      end

      context 'has uppercase letters' do
        let(:material) { 'Electronics' }
        it { should_not raise_error }
      end

      context 'other' do
        let(:material) { 'Plutonium-238' }
        it { should_not raise_error }
      end
    end

    context 'invalid material' do
      context 'not string' do
        let(:material) { 1.244123 }
        it { should raise_error(ArgumentError, 'Material must be a string.') }
      end
    end

  end
end

