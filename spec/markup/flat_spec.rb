require 'spec_helper'

describe Markup::Flat do
  let(:markup) { described_class.new(base_cost: base_cost) }
  let(:expected_percentage) { 0.05 }

  describe '#percentage' do
    subject { markup.percentage }

    context 'float' do
      let(:base_cost) { 1.244123 }
      it { should eq(expected_percentage) }
    end

    context 'integer' do
      let(:base_cost) { 4 }
      it { should eq(expected_percentage) }
    end

    context 'zero' do
      let(:base_cost) { 0 }
      it { should eq(expected_percentage) }
    end
  end

  describe '#validate' do
    subject { lambda { markup.validate } }

    context 'valid base_cost' do
      context 'float' do
        let(:base_cost) { 1.244123 }
        it { should_not raise_error }
      end

      context 'integer' do
        let(:base_cost) { 4 }
        it { should_not raise_error }
      end

      context 'zero' do
        let(:base_cost) { 0 }
        it { should_not raise_error }
      end
    end

    context 'invalid base_cost' do
      context 'negative' do
        let(:base_cost) { -1 }
        it { should raise_error(ArgumentError, 'Base Cost must be greater than or equal to zero.') }
      end

      context 'nil' do
        let(:base_cost) { nil }
        it { should raise_error(ArgumentError, 'Base Cost must be a number.') }
      end

      context 'not numeric' do
        let(:base_cost) { 'base_cost_string' }
        it { should raise_error(ArgumentError, 'Base Cost must be a number.') }
      end
    end

  end
end

