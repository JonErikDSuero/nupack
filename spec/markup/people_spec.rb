require 'spec_helper'

describe Markup::People do
  let(:markup) { described_class.new(people_count: people_count) }

  describe '#percentage' do
    subject { markup.percentage }

    context 'integer' do
      let(:people_count) { 4 }
      it { should eq(0.048) }
    end
  end

  describe '#validate' do
    subject { lambda { markup.validate } }

    context 'valid people_count' do
      context 'integer' do
        let(:people_count) { 4 }
        it { should_not raise_error }
      end
    end

    context 'invalid people_count' do
      context 'zero' do
        let(:people_count) { 0 }
        it { should raise_error(ArgumentError, 'Number of People must be greater than zero.') }
      end

      context 'float' do
        let(:people_count) { 1.244123 }
        it { should raise_error(ArgumentError, 'Number of People must be a whole number.') }
      end

      context 'negative' do
        let(:people_count) { -1 }
        it { should raise_error(ArgumentError, 'Number of People must be greater than zero.') }
      end

      context 'nil' do
        let(:people_count) { nil }
        it { should raise_error(ArgumentError, 'Number of People must be a whole number.') }
      end

      context 'not numeric' do
        let(:people_count) { 'people_count_string' }
        it { should raise_error(ArgumentError, 'Number of People must be a whole number.') }
      end
    end

  end
end

